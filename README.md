# Posts, Parsing, and Publication!

This is where I have what can only be described a rudimentary CMS for my website. 
- I store my blog posts here in markdown format (posts)
- I have my python code for parsing the posts into HTML for the website (parsing)
- I have my golang code that runs on a docker image that publishes new posts to the RSS feed and newsletter (publication)

Work here is licensed very similarly to the "www" repository. The code written and created by me is licensed under AGPLv3.0, the blog posts stored in a "markdown" format are the sole intellectual property of Mark Muchane and are copyrighted unless otherwise noted.
